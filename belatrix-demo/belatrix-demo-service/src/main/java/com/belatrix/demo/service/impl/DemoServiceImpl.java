package com.belatrix.demo.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.belatrix.demo.component.FileProcessComponent;
import com.belatrix.demo.model.Pattern;
import com.belatrix.demo.repository.FileRepository;
import com.belatrix.demo.repository.PatternRepository;
import com.belatrix.demo.service.DemoService;

@Service
public class DemoServiceImpl implements DemoService {
	
	@Autowired
	private FileRepository repository;
	
	@Autowired
	private PatternRepository patternRepo;
	
	@Autowired
	private FileProcessComponent component;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.belatrix.demo.service.DemoService#sayHi()
	 */
	@Override
	public String sayHi() {
		return "Greetings from Spring Boot!";
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.belatrix.demo.service.DemoService#saveFile(java.io.File, com.belatrix.demo.enums.PatternEnum)
	 */
	@Override
	public com.belatrix.demo.model.File saveFile(File fileToLoad, String patternId) throws IOException {
		com.belatrix.demo.model.File fileModel = createFileModel(fileToLoad, patternId);
		return repository.save(fileModel);
	}
	
	private com.belatrix.demo.model.File createFileModel(File fileToLoad, String patternId) throws IOException {
		Pattern pattern = patternRepo.findOne(patternId);
		
		com.belatrix.demo.model.File file = new com.belatrix.demo.model.File();
		file.setName(fileToLoad.getName());
		file.setUrls(Files.readAllLines(fileToLoad.toPath()));
		file.setCreatedDate(new Date());
		file.setStatus("Pending");
		file.setPattern(pattern);
		
		return file;
	}

	/*
	 * (non-Javadoc)
	 * @see com.belatrix.demo.service.DemoService#processFile(java.lang.String, java.lang.String)
	 */
	@Override
	public com.belatrix.demo.model.File processFile(String fileId, String patternId) throws Exception {
		String defaultFilePath = "C:\\belatrix-demo\\input\\siteList.txt";
		com.belatrix.demo.model.File fileToProcess = repository.findOne(fileId);
		
		if(fileId == null || fileToProcess == null) {
			File defaultFile = new File(defaultFilePath);
			
			if(defaultFile.exists()) {
				fileToProcess = createFileModel(defaultFile, patternId);
				repository.save(fileToProcess);
				defaultFile.delete();
			} else {
				throw new Exception("There is not file to proccess!");
			}
		}
		
		try {
			updateFileStatus(fileToProcess, "In progress");

			component.process(fileToProcess);
			
			updateFileStatus(fileToProcess, "Done");
		} catch (Exception e) {
			updateFileStatus(fileToProcess, "Error");
			throw e;
		}
		
		return fileToProcess;
	}

	private void updateFileStatus(com.belatrix.demo.model.File fileToProcess, String status) {
		fileToProcess.setStatus(status);
		repository.save(fileToProcess);
	}

	@Override
	public List<com.belatrix.demo.model.File> getAllFiles() {
		return repository.findAll();
	}

	@Override
	public void savePattern(Pattern pattern) {
		patternRepo.save(pattern);
	}

	@Override
	public List<Pattern> getAllPatterns() {
		return patternRepo.findAll();
	}

}
