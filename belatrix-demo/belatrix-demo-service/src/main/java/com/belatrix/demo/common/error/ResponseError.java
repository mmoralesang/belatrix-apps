package com.belatrix.demo.common.error;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseError implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String SUCCESS = "success";

	public static final String ERROR = "error";
	
	private String errorCode;
	private String httpStatus;
	private String errorMessage;
	private String rootErrorMessage;
	private List<ErrorBean> errorList;
	
	public ResponseError(String code,String errorMessage,String rootErrorMessage,List<ErrorBean> errors,String httpStatus) {
		super();
		this.errorCode = code;
		this.errorMessage = errorMessage;
		this.errorList = errors;
		this.rootErrorMessage = rootErrorMessage;
		this.httpStatus = httpStatus;
	}

	public ResponseError() {
		errorCode = SUCCESS;
		errorMessage = null;
		errorList = new ArrayList<>();
	}
	public ResponseError(String code) {
		this.errorCode = code;
		errorMessage = null;
		this.errorList = new ArrayList<>();
	}
	public ResponseError(String code,String errorMessage,String rootErrorMessage) {
		this.errorCode = code;
		this.errorMessage = errorMessage;
		this.rootErrorMessage = rootErrorMessage;
		this.errorList = new ArrayList<>();
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getRootErrorMessage() {
		return rootErrorMessage;
	}

	public void setRootErrorMessage(String rootErrorMessage) {
		this.rootErrorMessage = rootErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<ErrorBean> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<ErrorBean> errorList) {
		this.errorList = errorList;
	}
	
	/**
	 * Utility method that add an error to the list, but initializing it if is
	 * required
	 * 
	 * @param errorCode
	 * @param errorMessage
	 */
	public void addError(String errorCode, String errorMessage) {
		addError(new ErrorBean(errorCode, errorMessage));
		errorCode = ERROR;
	}
	
	public void addError(String field,String errorCode,String errorMessage,String type) {
		addError(new ErrorBean(field,errorCode, errorMessage,type));
	}
	
	public void addError(ErrorBean error){
		getErrorList().add(error);
	}
}
