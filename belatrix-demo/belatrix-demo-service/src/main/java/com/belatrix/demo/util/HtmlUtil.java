package com.belatrix.demo.util;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * 
 * @author Marcos
 *
 */
public class HtmlUtil {

	public static String readBodyContent(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		Elements body = doc.select("body");
		return body.text();
	}

}
