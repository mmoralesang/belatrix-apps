package com.belatrix.demo.common.type;

public enum ErrorType {
	INVALID_REQUEST("ERR_INVALID_REQUEST","Bad Request"),
	LOW_BUSINESS_EXCEPTION("LOW_BUSINESS_EXCEPTION","Business Validation"),
	HIGH_BUSINESS_EXCEPTION("HIGH_BUSINESS_EXCEPTION","Business Validation"),
	SYSTEM_EXCEPTION("SYSTEM_EXCEPTION","System Exception");
	
	private String code;
	private String name;
	
	ErrorType(String code,String name){
		this.code=code;
		this.name=name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString(){
		return code;
	}
	
	
}
