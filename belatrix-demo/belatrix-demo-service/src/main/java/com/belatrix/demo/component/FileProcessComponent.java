package com.belatrix.demo.component;

import com.belatrix.demo.model.File;

public interface FileProcessComponent {

	/**
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	java.io.File process(File file) throws Exception;

}
