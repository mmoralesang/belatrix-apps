package com.belatrix.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.belatrix.demo.model.File;

@Repository
public interface FileRepository extends MongoRepository<File, String>{

}
