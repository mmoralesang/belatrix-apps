package com.belatrix.demo.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.belatrix.demo.common.error.ErrorBean;
import com.belatrix.demo.common.error.ResponseError;
import com.belatrix.demo.common.exception.BusinessException;
import com.belatrix.demo.common.type.ErrorType;
import com.fasterxml.jackson.databind.ObjectMapper;



public class CommonUtil {

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	public static String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public static String getHttpStatus(String code, Exception ex) {
		if (StringUtils.isBlank(code)) {

			return (ex instanceof BusinessException) ? HttpStatus.BAD_REQUEST
					.toString() : HttpStatus.INTERNAL_SERVER_ERROR.toString();
		} else {
			try (Stream<HttpStatus> stream = Arrays.stream(HttpStatus.values())) {
				HttpStatus status = stream
						.filter(e -> e.value() == Integer.parseInt(code))
						.findAny()
						.orElseThrow(
								() -> new IllegalStateException(String.format(
										"Unsupported type %s.", code)));
				return status.toString();
			} catch (NumberFormatException | IllegalStateException e) {
				return HttpStatus.INTERNAL_SERVER_ERROR.toString();
			}

		}
	}
	
	/**
	 * Return a ResponseEntity with ResponseError body for implicit exceptions handled by Spring that will be @Override
	 * @param httpStatus HttpStatus that will return
	 * @param ex Spring MVC exception
	 * @param headers Headers that will return
	 */
	public static ResponseEntity<Object> createCommonResponse(
			HttpStatus httpStatus,	ServletException ex, HttpHeaders headers) {
		return createResponseError(ex, httpStatus.toString(), ErrorType.INVALID_REQUEST,
				null, headers);
	}
	
	/**
	 * Return a ResponseEntity with ResponseError body for declarative exceptions thrown by @Controller classes.
	 * @param ex BusinessException or SystemException are expected.
	 * @param code HttpStatus code converted in String 
	 * @param errorType ErrorType.BUSINESS_EXCEPTION or ErrorType.SYSTEM_EXCEPTION are expected.
	 */
	public static ResponseEntity<Object> createResponseError(Exception ex,
			String code, ErrorType errorType) {
		return createResponseError(ex, code, errorType, null);
	}
	
	/**
	 * Return a ResponseEntity with ResponseError body for declarative exceptions thrown by @Controller classes.
	 * @param ex BusinessException or SystemException are expected.
	 * @param code HttpStatus code converted in String 
	 * @param errorType ErrorType.BUSINESS_EXCEPTION or ErrorType.SYSTEM_EXCEPTION are expected.
	 * @param errorBeans A ErrorBean array is expected
	 */
	public static ResponseEntity<Object> createResponseError(Exception ex,
			String code, ErrorType errorType, List<ErrorBean> errorBeans) {
		return createResponseError(ex, code, errorType, errorBeans, null);
	}

	private static ResponseEntity<Object> createResponseError(Exception ex,
			String code, ErrorType errorType, List<ErrorBean> errorBeans,
			HttpHeaders headers) {
		String httpStatusStr = getHttpStatus(code, ex);
		HttpStatus httpStatus = null;
		if (!httpStatusStr.isEmpty())
			httpStatus = HttpStatus.valueOf(Integer.parseInt(httpStatusStr));

		return new ResponseEntity<Object>(returnBodyResponseError(ex,
				httpStatus, httpStatusStr, errorType, errorBeans), headers,
				httpStatus);
	}

	private static Object returnBodyResponseError(Exception ex, HttpStatus httpStatus,
			String httpStatusStr, ErrorType errorType,
			List<ErrorBean> errorBeans) {
		ResponseError responseError = new ResponseError(ResponseError.ERROR,
				ex.getLocalizedMessage(), ex.getMessage());
		try {
			responseError.setErrorMessage(httpStatus.getReasonPhrase() + ". "
					+ responseError.getErrorMessage());
			responseError.setHttpStatus(httpStatusStr);
			responseError.setErrorCode(errorType.getCode());
			responseError.setErrorList(errorBeans != null ? errorBeans
					: new ArrayList<>());
			return new ObjectMapper()
					.writeValueAsString(responseError != null ? responseError
							: "");
		} catch (Exception exa) {
			return "";
		}
	}
}
