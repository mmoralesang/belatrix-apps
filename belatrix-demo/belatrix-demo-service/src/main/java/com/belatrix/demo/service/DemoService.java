package com.belatrix.demo.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.belatrix.demo.model.Pattern;

public interface DemoService {
	
	/**
	 * 
	 * @return
	 */
	public String sayHi();
	
	/**
	 * 
	 * @param fileId
	 * @param patternId 
	 * @return
	 * @throws Exception
	 */
	public com.belatrix.demo.model.File processFile(String fileId, String patternId) throws Exception;

	/**
	 * 
	 * @param fileToLoad
	 * @param patternId
	 * @return
	 * @throws IOException
	 */
	public com.belatrix.demo.model.File saveFile(File fileToLoad, String patternId) throws IOException;

	/**
	 * 
	 * @return
	 */
	public List<com.belatrix.demo.model.File> getAllFiles();

	/**
	 * 
	 * @param pattern
	 */
	public void savePattern(Pattern pattern);

	/**
	 * 
	 * @return
	 */
	public List<Pattern> getAllPatterns();

}
