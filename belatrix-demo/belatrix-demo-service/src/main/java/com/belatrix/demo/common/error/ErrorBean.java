package com.belatrix.demo.common.error;

import java.io.Serializable;
import java.util.Objects;
import static com.belatrix.demo.common.util.CommonUtil.toIndentedString;

public class ErrorBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String field;
	private String code;
	private String message;
	private String errorType;
	
	public ErrorBean() {
	}

	public ErrorBean(String field,String code, String message,String errorType){
		super();
		setCode(code);
		setField(field);
		setMessage(message);
		setErrorType(errorType);
		
	}
	
	public ErrorBean(String code, String message,String errorType){
		super();
		setCode(code);
		setMessage(message);
		setErrorType(errorType);
	}
	
	public ErrorBean(String message, String errorType){
		super();
		setMessage(message);
		setErrorType(errorType);
	}
 
	@Override
	public int hashCode() {
		return Objects.hash(code,message, errorType);
	}

	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorBean object = (ErrorBean) o;
		return Objects.equals(code, object.code) 
				&& Objects.equals(message, object.message)
				&& Objects.equals(errorType, object.errorType);
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorBean {\n");
		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    errorType: ").append(toIndentedString(errorType)).append("\n");
		sb.append("}");
		return sb.toString();
		
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	
	
}
