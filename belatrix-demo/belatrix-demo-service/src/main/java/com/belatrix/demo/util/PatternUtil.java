/**
 * 
 */
package com.belatrix.demo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Marcos
 *
 */
public class PatternUtil {

	/**
	 * 
	 * @param str
	 * @param regex
	 * @return
	 */
	public static List<String> parse(String str, String regex) {
		List<String> result = new ArrayList<>();
		Pattern patt = Pattern.compile(regex);
		Matcher matcher = patt.matcher(str);

		while (matcher.find()) {
			result.add(matcher.group(1));
		}

		return result;
	}

}
