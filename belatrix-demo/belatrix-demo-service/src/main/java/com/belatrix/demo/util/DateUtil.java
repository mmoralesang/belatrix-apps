package com.belatrix.demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static String formatAs(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

}
