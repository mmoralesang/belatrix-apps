/**
 * 
 */
package com.belatrix.demo.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.belatrix.demo.model.Pattern;

/**
 * @author Marcos
 *
 */
public final class ProcessUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessUtil.class);
	
	private static void processURL(String url, Pattern pattern, String path) {
		String htmlBodyContent = null;
		List<String> result = null;
		String fileNamePrefix = path + "/" + System.currentTimeMillis() + "" + new Random().nextInt();
		String fileNameSufix  = ".txt";
		String fileNameErrorSufix  = "_ERROR.txt";
		String fileName;

		try {
			fileName = fileNamePrefix + fileNameSufix;
			
			// Read URL
			htmlBodyContent = HtmlUtil.readBodyContent(url);
			LOGGER.debug("Body content: " + htmlBodyContent);

			// Look for pattern
			result = PatternUtil.parse(htmlBodyContent, pattern.getRegex());
			LOGGER.debug("result: " + result.toString());

			// generate result file
			writeOutputFile(fileName, url, result);
			LOGGER.debug("ouput file: " + fileName);
		} catch (Exception ex) {
			String errorMessage = "An error occurred when proccessing URL: " + url;
			LOGGER.error(errorMessage);
			errorMessage = errorMessage + "\n" + ex.getMessage();
			fileName = fileNamePrefix + fileNameErrorSufix;
			
			try {
				Files.write(Paths.get(fileName), Arrays.asList(errorMessage), Charset.defaultCharset());
			} catch(IOException e) {
				LOGGER.error("An error occurred when writting error file: " + fileName);
			}
		}
	}
	
	private static void writeOutputFile(String outputFile, String url, List<String> result) throws IOException {
		result.add(0, "Matches in URL(" + url + "): " + result.size());
		Files.write(Paths.get(outputFile), result, Charset.defaultCharset());
	}

	public static class RunnableProcessFile implements Runnable {
		private static final Logger LOGGER = LoggerFactory.getLogger(RunnableProcessFile.class);
		
		private String url;
		private Pattern pattern;
		private String path;
		
		public RunnableProcessFile(String url, Pattern pattern, String path) {
			this.url = url;
			this.pattern = pattern;
			this.path =  path;
		}

		@Override
		public void run() {
			LOGGER.info("***** Start process URL: " + url);
			processURL(url, pattern, path);
			LOGGER.info("***** End process URL: " + url);
		}
		
	}

}