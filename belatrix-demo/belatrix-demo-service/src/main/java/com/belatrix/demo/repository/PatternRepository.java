package com.belatrix.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.belatrix.demo.model.Pattern;

@Repository
public interface PatternRepository extends MongoRepository<Pattern, String>{

}
