package com.belatrix.demo.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelatrixDemoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelatrixDemoServiceApplication.class, args);
	}
}
