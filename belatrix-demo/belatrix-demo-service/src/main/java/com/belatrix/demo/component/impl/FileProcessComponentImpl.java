/**
 * 
 */
package com.belatrix.demo.component.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.belatrix.demo.component.FileProcessComponent;
import com.belatrix.demo.model.File;
import com.belatrix.demo.util.ProcessUtil;
import com.belatrix.demo.util.ZipUtil;

/**
 * @author Marcos
 *
 */
@Component
public class FileProcessComponentImpl implements FileProcessComponent{
	private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessComponentImpl.class);
	
	/*
	 * (non-Javadoc)
	 * @see com.belatrix.demo.component.FileProcessComponent#process(com.belatrix.demo.model.File)
	 */
	@Override
	public java.io.File process(File file) throws Exception {
		// Validate required
		this.validate(file);
		
		// process
		return this.doProcess(file);
	}
	
	private void validate(File file) {
		if(file == null) {
			throw new RuntimeException("File document is required!");
		}
		
		if (file.getUrls() == null || file.getUrls().isEmpty()) {
			throw new RuntimeException("URL list is required!");
		}

		if (file.getPattern() == null) {
			throw new RuntimeException("Pattern is required!");
		}
		
	}

	private java.io.File doProcess(File file) throws Exception {
		// Create a temporary folder
		//String targetFolder = "/opt/belatrix/belatrix-demo/output";
		String targetFolder = "C:\\belatrix-demo\\output";
		String targetTmpParentFolder = targetFolder + "\\tmp\\";
		String targetTmpFolder = targetFolder + "\\tmp\\" + file.getId();
		
		java.io.File tmpParentFolder = new java.io.File(targetTmpParentFolder);
		if(!tmpParentFolder.exists()) {
			tmpParentFolder.mkdir();
		}
		
		Path tmpFolder = Files.createDirectory(Paths.get(targetTmpFolder));
		
		// Process URL list
		ExecutorService taskExecutor = Executors.newFixedThreadPool(50);
		file.getUrls().forEach(url -> {
			Runnable task = new ProcessUtil.RunnableProcessFile(url, file.getPattern(), tmpFolder.toString());
			taskExecutor.execute(task);
		});
		
		taskExecutor.shutdown();
		
		//taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		while (!taskExecutor.isTerminated()) {}
		
		// Zip temporary folder
		java.io.File zipOutputFile = ZipUtil.zip(tmpFolder, targetFolder);
		LOGGER.info("Zip output file: " + zipOutputFile.getPath());
		
		return zipOutputFile;
	}
	
}
