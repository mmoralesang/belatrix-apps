package com.belatrix.demo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 
 * @author Marcos
 *
 */
public class ZipUtil {

	/**
	 * 
	 * @param sourcePath
	 * @param targetFolder
	 * @return
	 * @throws IOException
	 */
	public static File zip(Path sourcePath, String targetFolder) throws IOException {
		File zipFile = new File(targetFolder + "/" + sourcePath.getFileName().toString() + ".zip");
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		
		File parent = zipFile.getParentFile();
		if(!parent.exists()) {
			parent.mkdirs();
		}
		
		if(zipFile.createNewFile()) {
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(fos);
			
			zipFile(sourcePath.toFile(), sourcePath.getFileName().toString(), zos);
			
			zos.close();
			fos.close();
		}
		
		return zipFile;
	}

	private static void zipFile(File fileToZip, String fileName, ZipOutputStream zos) throws IOException {
		if(fileToZip.isDirectory()) {
			File[] children = fileToZip.listFiles();
			
			for (File child : children) {
				zipFile(child, fileName + "/" + child.getName(), zos);
			}
			return;
		}
		
		FileInputStream fis = new FileInputStream(fileToZip);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
        fis.close();
	}

}
