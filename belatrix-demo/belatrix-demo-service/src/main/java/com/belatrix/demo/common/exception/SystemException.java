package com.belatrix.demo.common.exception;

public class SystemException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String message;
	private Exception rootCause;

	public SystemException(String message) {
		
		super(message);
		this.message = message;
	}

	public SystemException(String code,String message) {
		
		super(message);
		this.code = code;
		this.message = message;
	}
	public SystemException(Exception rootCause) {
		
		this.rootCause = rootCause;
		this.message = rootCause.getMessage();
	}

	public SystemException(String message, Exception rootCause) {
		
		
		super(message, rootCause);
		this.message = message;
		this.rootCause = rootCause;
	} 

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {		
		this.message = message;
	}

	public Exception getRootCause() {
		return rootCause;
	}

	public void setRootCause(Exception rootCause) {
		this.rootCause = rootCause;
	}

}
