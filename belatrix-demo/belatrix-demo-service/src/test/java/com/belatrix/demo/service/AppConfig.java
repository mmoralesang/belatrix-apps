package com.belatrix.demo.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.belatrix.demo.service"})
public class AppConfig {

}
