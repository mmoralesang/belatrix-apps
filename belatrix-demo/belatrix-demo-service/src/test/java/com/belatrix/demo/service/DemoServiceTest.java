package com.belatrix.demo.service;

import com.belatrix.demo.model.Pattern;
import com.belatrix.demo.service.impl.DemoServiceImpl;

public class DemoServiceTest {
	private DemoService service = new DemoServiceImpl();
	
	//private static List<String> urls = new ArrayList<>(); 
	
	public static void main(String []args) {
		DemoServiceTest test = new DemoServiceTest();
		//test.processTest();
		test.createPatternTest();
	}
	
	private void createPatternTest() {
		Pattern pattern = new Pattern();
		pattern.setName("HASHTAG");
		pattern.setValue("#");
		pattern.setRegex("\\\\B(\\\\#[a-zA-Z]+\\\\b)(?!;)");
		
		Pattern twitter = new Pattern();
		twitter.setName("TWITTER_ACCOUNT");
		twitter.setValue("@");
		twitter.setRegex("@([A-Za-z0-9_]+)");
		
		
		service.savePattern(pattern);
		service.savePattern(twitter);
	}

	public void processTest() {
		/*urls.add("http://mashable.com/2013/10/08/what-is-hashtag/#PS1urW4pIPqg");
		urls.add("http://www.yahoo.com");
		urls.add("http://www.cnn.com");
		urls.add("http://news.google.com");
		urls.add("http://www.huffingtonpost.com/");
		urls.add("http://www.nytimes.com/");
		urls.add("http://www.nbcnews.com/");
		urls.add("http://www.foxnews.com/");
		urls.add("http://ww.boston.com/");
		urls.add("http://www.theguardian.com/us");
		urls.add("http://www.usatoday.com/");
		urls.add("http://www.washingtonpost.com/");
		urls.add("http://www.bbc.co.uk/news/");
		urls.add("http://abcnews.go.com/");
		urls.add("http://www.latimes.com/");
		urls.add("http://www.cbsnews.com/");
		urls.add("http://www.reuters.com/");
		urls.add("http://www.rollingstones.com/");
		urls.add("http://news.cnet.com/");
		urls.add("http://www.zdnet.com/");
		urls.add("http://www.extremetech.com/");
		urls.add("http://www.technewsworld.com/");
			
		try {
			service.process(urls, PatternEnum.HASHTAG);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
	}
	

}
