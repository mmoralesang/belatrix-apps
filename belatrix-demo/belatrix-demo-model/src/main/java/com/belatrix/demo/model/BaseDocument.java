package com.belatrix.demo.model;

import java.util.Date;
import java.util.Objects;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;


/**
 * Base class for document classes.
 * 
 */
public class BaseDocument implements Persistable<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1732913368772656881L;

	protected static final String ISO_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	
	@Id
	private String id;
	
	@CreatedDate
    private Date createdDate;
	
    @LastModifiedDate
    private Date lastModifiedDate;
    
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the identifier of the document.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		BaseDocument that = (BaseDocument) obj;
		return Objects.equals(id, that.id)
				&& Objects.equals(createdDate, that.createdDate)
				&& Objects.equals(lastModifiedDate, that.lastModifiedDate);
		 
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id, createdDate, lastModifiedDate);
	}
	
	@Override
    public boolean isNew() {
        return id==null;
    }

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
