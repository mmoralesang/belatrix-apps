package com.belatrix.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Pattern extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 11808347599411821L;
	
	private String name;
	private String value;
	private String regex;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}
	
}
