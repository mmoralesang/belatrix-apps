/**
 * 
 */
package com.belatrix.demo.model.converter;

import java.util.Map;

import org.springframework.core.convert.converter.Converter;

import com.belatrix.demo.model.Pattern;
import com.belatrix.demo.model.util.JsonUtils;

/**
 * @author Marcos
 *
 */
public class PatternConverter implements Converter<String, Pattern> {

	/* (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Pattern convert(String source) {
		Map<String, String> patternMap = JsonUtils.deserialize(source, Map.class);
		
		Pattern pattern = null;
		if(patternMap != null) {
			pattern = new Pattern();
			pattern.setId(patternMap.get("id").toString());
			pattern.setName(patternMap.get("name").toString());
			pattern.setValue(patternMap.get("value").toString());
			pattern.setRegex(patternMap.get("regex").toString());
		}
		
		return pattern;
	}

}
