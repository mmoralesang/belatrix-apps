/**
 * 
 */
package com.belatrix.demo.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Marcos
 *
 */
@Document
public class File extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8320243751890678673L;

	private String name;
	private List<String> urls;
	@DBRef(lazy=true)
	private Pattern pattern;
	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}
	
	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
