/**
 * 
 */
package com.belatrix.demo.model.util;

import java.io.IOException;

import com.dslplatform.json.DslJson;
import com.dslplatform.json.JsonWriter;

/**
 * @author Marcos
 *
 */
public final class JsonUtils {
	
	@SuppressWarnings("unchecked")
	public static <T> T deserialize(String value, Class<?> clazz) {
		DslJson<T> dslJson = new DslJson<T>();			
		//JsonWriter writer = dslJson.newWriter();		
		try {
			return (T) dslJson.deserialize(clazz, value.getBytes(),	value.getBytes().length);
		} catch (IOException e) {	
			return null;
		}	
	}

}
