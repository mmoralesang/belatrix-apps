package com.belatrix.demo.enums;

public enum PatternEnum {
	HASHTAG("#", "\\B(\\#[a-zA-Z]+\\b)(?!;)"), TWITTER_ACCOUNT("@", "@([A-Za-z0-9_]+)"), PROPER_NAME("?", "???"); 
	
	private String value;
	private String regex;
	PatternEnum(String value, String regex) {
		this.value = value;
		this.regex = regex;
	}
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
