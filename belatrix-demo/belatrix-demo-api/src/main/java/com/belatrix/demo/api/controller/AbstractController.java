package com.belatrix.demo.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.belatrix.demo.common.error.ResponseError;
import com.belatrix.demo.common.exception.BusinessException;
import com.belatrix.demo.common.exception.SystemException;

import static com.belatrix.demo.common.util.CommonUtil.getHttpStatus;


@CrossOrigin(origins = "*")
public abstract class AbstractController extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(value = { BusinessException.class, SystemException.class})
    protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {
       
    	ResponseError responseError = new ResponseError(ResponseError.ERROR,ex.getLocalizedMessage(),ex.getMessage());
        
    	String code = "";
    	if(ex instanceof BusinessException){
    		BusinessException serviceException = (BusinessException)ex;
    		code = serviceException.getCode();	
    		responseError.setErrorCode(serviceException.getType());
    		if(serviceException.getErrorList()!=null){
    			responseError.setErrorList(serviceException.getErrorList());
    		}
    	}
    	else if(ex instanceof SystemException){
    		code = ((SystemException)ex).getCode();
    		responseError.setErrorCode(code);
    	}
		
		responseError.setHttpStatus(getHttpStatus(code,ex));
		
		return new ResponseEntity<>(responseError,
					HttpStatus.valueOf(Integer.parseInt(responseError.getHttpStatus())));
    }
    
}    

