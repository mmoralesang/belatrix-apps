package com.belatrix.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//@EnableAspectJAutoProxy
@Configuration
@EnableAutoConfiguration
@EnableMongoAuditing
@RefreshScope
@EnableMongoRepositories(basePackages = { "com.belatrix.demo.repository" })
@ComponentScan(basePackages = { "com.belatrix.demo" })
//@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
