package com.belatrix.demo.api.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.belatrix.demo.common.exception.BusinessException;
import com.belatrix.demo.model.Pattern;
import com.belatrix.demo.service.DemoService;

@RestController
@RequestMapping("/api/demo")
public class DemoController extends AbstractController {
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoController.class);
	
	@Autowired
	private DemoService service;

	@RequestMapping(value = "/pattern", method = RequestMethod.POST)
	public ResponseEntity<Pattern> createRepository(@RequestBody Pattern pattern) {
		service.savePattern(pattern);
		return new ResponseEntity<Pattern>(pattern, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/pattern/list", method = RequestMethod.GET)
	public ResponseEntity<List<Pattern>> getAllPatterns() {
		return new ResponseEntity<List<Pattern>>(service.getAllPatterns(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	public ResponseEntity<List<com.belatrix.demo.model.File>> getAllFiles() {
		List<com.belatrix.demo.model.File> files = service.getAllFiles();
		return new ResponseEntity<List<com.belatrix.demo.model.File>>(files, HttpStatus.OK);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String handleFileUpload(@RequestParam("patternId") String patternId,  
			@RequestParam("file") MultipartFile file) {
		if (!file.isEmpty()) {
			try {
				File fileToLoad = new File(file.getOriginalFilename());
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileToLoad));
				stream.write(bytes);
				stream.close();
				
				com.belatrix.demo.model.File uploadedFile = service.saveFile(fileToLoad, patternId);
				
				return "Process file: <a href='processFile/" + uploadedFile.getId() + "'>" + uploadedFile.getName() + "</a>";
				//return "Process file: <a href='processFile'>" + name + "</a>";
			} catch (Exception e) {
				return "You failed to upload " + file.getName() + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + file.getName() + " because the file was empty.";
		}
	}
	
	@RequestMapping(value = "/processFile/{fileId}/{patternId}", method = RequestMethod.POST /*, produces="application/zip"*/)
	public ResponseEntity<?> processFile(@PathVariable("fileId") String fileId, @PathVariable("patternId") String patternId,
			HttpServletResponse response) {
		Map<String, Object> objResponse = new HashMap<>();

		try {
			com.belatrix.demo.model.File outputFile = service.processFile(fileId, patternId);

			return new ResponseEntity<com.belatrix.demo.model.File>(outputFile, HttpStatus.OK);
			//return downloadZipFile(response, outputFile);
		} catch (BusinessException e) {
			LOGGER.error("Api Exception", e);
			objResponse.put("result", "ERROR");
			objResponse.put("errorMessage", e.getStackTrace());
			return new ResponseEntity<Map<String, Object>>(objResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			LOGGER.error("Unexpected Exception", e);
			objResponse.put("result", "ERROR");
			objResponse.put("errorMessage", e.getStackTrace());
			return new ResponseEntity<Map<String, Object>>(objResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private byte[] downloadZipFile(HttpServletResponse response, File outputFile) throws IOException {
		response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=\"" + outputFile.getName() + "\"");
        Path path = outputFile.toPath();
		return Files.readAllBytes(path);
	}

}
