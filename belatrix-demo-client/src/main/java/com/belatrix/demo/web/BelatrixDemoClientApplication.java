package com.belatrix.demo.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelatrixDemoClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelatrixDemoClientApplication.class, args);
	}
}
