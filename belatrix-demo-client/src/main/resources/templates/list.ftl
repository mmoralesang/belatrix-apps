<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Belarix Demo - Process File </span></div>
		<div class="panel-body">
	        <div class="formcontainer">
	            <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
	            <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
	            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.user.id" />
	                
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="uname">File to Process:</label>
	                        <div class="col-md-7">
	                            <!-- input type="text" ng-model="ctrl.filePath" id="uname" class="username form-control input-sm" required ng-minlength="10" readonly="true"/ -->
	                            C:\\belatrix-demo\\input\\siteList.txt	                            
	                        </div>
	                    </div>
	                </div>
	                
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="uname">Pattern</label>
	                        <div class="col-md-7">
	                            <select ng-model="ctrl.selectedPattern" ng-options="item.id as item.name for item in ctrl.getAllPatterns()" class="username form-control select-sm">
    								<option value="">Select Pattern</option>
								</select>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">
	                        <!-- input type="submit"  value="{{!ctrl.link.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid || myForm.$pristine" -->
	                        <button type="button" ng-click="ctrl.processAllSearch(ctrl.selectedPattern)" class="btn btn-sm custom-width">Process</button>
	                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>	                        
	                        <!-- button type="button" ng-click="ctrl.processMultiSearch(ctrl.selectedPattern)" class="btn btn-sm custom-width">Parallel</button -->
	                    </div>
	                </div>
	            </form>
    	    </div>
		</div>	
    </div>
    
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Proccessed Files </span></div>
		<div class="panel-body">
			<div class="table-responsive">
		        <table class="table table-hover">
		            <thead>
		            <tr>
		                <th>ID</th>
		                <th>File Name</th>
		                <th>Pattern</th>
		                <th>Status</th>
		                <th>Actions</th>
		                <th width="100"></th>
		            </tr>
		            </thead>
		            <tbody>
		            <tr ng-repeat="f in ctrl.getFiles()">
		                <td>{{f.id}}</td>
		                <td>{{f.name}}</td>
		                <td>{{f.pattern.name}}</td>
		                <td>{{f.status}}</td>
		                <td>
		                	<td><button type="button" ng-click="ctrl.selectFile(f)" class="btn btn-danger custom-width">{urls}</button></td>
		                </td>
		                <!-- td><button type="button" ng-click="ctrl.processLinkSearch(u.id)" class="btn btn-danger custom-width">Process</button></td -->
		            </tr>
		            </tbody>
		        </table>		
			</div>
		</div>
    </div>
    
</div>