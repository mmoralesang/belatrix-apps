var app = angular.module('crudApp',['ui.router','ngStorage']);

app.constant('urls', {
    LINK_SERVICE_API : 'http://localhost:8080/api/demo/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/list',
                controller:'LinkController',
                controllerAs:'ctrl',
                resolve: {
                    links: function ($q, LinkService) {
                        console.log('Load demo form data');
                        var deferred = $q.defer();
                        LinkService.loadAllPatterns().then(deferred.resolve, deferred.resolve);
                        LinkService.loadFiles().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);

