'use strict';

angular.module('crudApp').controller('LinkController',
    ['LinkService', '$scope',  function( LinkService, $scope) {

        var self = this;
        self.filePath="C:\\belatrix-demo\\input\\siteList.txt";
        self.user = {};
        self.users=[];
        self.file={};
        self.files=[];
        self.links=[];
        self.link = {};
        self.selectedPattern = 1;
        self.patterns = [];
        self.submit = submit;
        self.getFiles = getFiles;
        self.getAllLinks = getAllLinks;
        self.getAllPatterns = getAllPatterns;
        self.createLink = createLink;
        self.reset = reset;        
        self.processLinkSearch = processLinkSearch;
        self.processAllSearch = processAllSearch;
        self.processMultiSearch = processMultiSearch;
        self.selectFile = selectFile;
        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            console.log('Submitting');
            if (self.link.id === undefined || self.link.id === null) {
                console.log('Saving New Link', self.link);
                createLink(self.link);
            } else {
                updateLink(self.link, self.link.id);
                console.log('Link updated with id ', self.link.id);
            }
        }
        
        function createLink(link) {
            console.log('About to create link');
            LinkService.createLink(link)
                .then(
                    function (response) {
                        console.log('Link created successfully');
                        self.successMessage = 'Link created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.link={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating Link');
                        self.errorMessage = 'Error while creating Link: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }
        
        function processLinkSearch(id){
            console.log('Process Link id '+id+ 'and Pattern id ' + self.selectedPattern);
            LinkService.processLinkSearch(id, self.selectedPattern)
                .then(
                    function(){
                        console.log('Link'+id + ' processed successfully');
                        self.successMessage='Process executed successfully';
                        self.errorMessage='';
                        self.done = true;
                    },
                    function(errResponse){
                        console.error('Error while processing link '+id +', Error :'+errResponse.data);
                    }
                );
        }
        
        function processAllSearch(id){
            console.log('Process all the file with PatternId: '+ id);
            LinkService.processAllSearch(id)
                .then(
                    function(){
                        console.log('All the file was processed successfully');
                        self.successMessage='Process performed successfully for the file';
                        self.errorMessage='';
                        self.done = true;
                    },
                    function(errResponse){
                        console.error('Error while processing file with PatternId:' + id +', Error :' + errResponse.data);
                    }
                );
        }
        
        function processMultiSearch(id){
            console.log('Process Multi Links with PatternId: '+id);
            LinkService.processMultiSearch(id)
                .then(
                    function(){
                        console.log('Multi Links processed successfully');
                        self.successMessage='Parallel process executed successfully for All sites';
                        self.errorMessage='';
                        self.done = true;
                    },
                    function(errResponse){
                        console.error('Error while processing multi links with PatternId:'+id +', Error :'+errResponse.data);
                    }
                );
        }
        
        function selectFile(f) {
        	return LinkService.getFile(f);
        }
        
        function getFiles() {
        	return LinkService.getFiles();
        }

        function getAllLinks(){
            return LinkService.getAllLinks();
        }
        
        function getAllPatterns(){
            return LinkService.getAllPatterns();
        }

        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.link={};
            $scope.myForm.$setPristine(); //reset Form
        }
    }


    ]);