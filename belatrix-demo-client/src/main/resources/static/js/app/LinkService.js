'use strict';

angular.module('crudApp').factory('LinkService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
            	loadFiles: loadFiles,
            	loadAllLinks: loadAllLinks,
                loadAllPatterns: loadAllPatterns,
                getFile: getFile,
                getFiles: getFiles,
                getAllLinks: getAllLinks,
                getAllPatterns: getAllPatterns,
                createLink: createLink,
                processLinkSearch: processLinkSearch,
                processAllSearch: processAllSearch,
                processMultiSearch: processMultiSearch
            };

            return factory;
            
            function loadFiles() {
            	console.log('Fetching all files');
            	var deferred = $q.defer();
                $http.get(urls.LINK_SERVICE_API + 'files')
                	.then(
                        function (response) {
                            console.log('Fetched successfully all files');
                            $localStorage.files = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('An error occurred when loading files');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function loadAllLinks() {
                console.log('Fetching all links');
                var deferred = $q.defer();
                $http.get(urls.LINK_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all links');
                            $localStorage.links = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading links');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function loadAllPatterns() {
                console.log('Fetching all patterns');
                var deferred = $q.defer();
                $http.get(urls.LINK_SERVICE_API + 'pattern/list')
                    .then(
                        function (response) {
                            console.log('Fetched successfully all patterns');
                            $localStorage.patterns = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading patterns');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function getFile(f){
            	$localStorage.file = f;
            	return $localStorage.file;
            }
            
            function getFiles(){
                return $localStorage.files;
            }

            function getAllLinks(){
                return $localStorage.links;
            }
            
            function getAllPatterns(){
                return $localStorage.patterns;
            }
            
            function createLink(link) {
                console.log('Creating Link');
                var deferred = $q.defer();
                $http.post(urls.LINK_SERVICE_API, link)
                    .then(
                        function (response) {
                        	loadAllLinks();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating link : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function processLinkSearch(id, patternId) {
                console.log('Process File id '+ id + ' and PatternId ' + patternId);
                var deferred = $q.defer();
                $http.post(urls.LINK_SERVICE_API + 'processFile/' + id + '/' + patternId)
                    .then(
                        function (response) {
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while processing File id : ' + id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function processAllSearch(id) {
                console.log('Process file with PatternId: ' + id);
                var deferred = $q.defer();
                $http.post(urls.LINK_SERVICE_API + 'processFile/' + null + '/' + id)
                    .then(
                        function (response) {
                        	loadFiles();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('An error occurred when processing file with PatternId: ' + id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function processMultiSearch(id) {
                console.log('Process Multi Search with PatternId '+id);
                var deferred = $q.defer();
                $http.post(urls.LINK_SERVICE_API + 'processAllMulti/'+id)
                    .then(
                        function (response) {
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while processing multi search with PatternId :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            

        }
    ]);